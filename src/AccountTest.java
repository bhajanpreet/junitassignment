import static org.junit.Assert.*;
import org.junit.Test;

public class AccountTest {

	@Test
	public void testAccountCreatesWithZeroBalance() {
		Account account = new Account(1000);
		assertEquals(0, account.balance());
	}

	@Test
	public void testDeposit() {
		Account account = new Account(1000);
		int balanceBefore = account.balance();
		account.deposit(100);
		int balanceAfter = account.balance();
		assertEquals(100, balanceAfter - balanceBefore);
	}

	@Test
	public void testWithDraw() {
		Account account = new Account(1000);
		account.deposit(100);

		int balanceBefore = account.balance();
		account.withdraw(100);
		int balanceAfter = account.balance();
		assertEquals(100, balanceBefore - balanceAfter);
	}

	@Test
	public void testWithDrawWithoutSufficientFunds() {
		Account account = new Account(1000);

		int balanceBefore = account.balance();
		account.withdraw(100000);
		int balanceAfter = account.balance();
		// If no withdraw occurs due to insufficient funds the
		// balance will be same as before
		assertEquals(balanceAfter, balanceBefore);
	}

	@Test
	public void testWithDrawOccursInstantly() {
		Account account = new Account(100);
		int balanceBefore = account.balance();
		account.withdraw(10);
		assertEquals(balanceBefore - 10, account.balance());
	}

	@Test
	public void testDepositOccursInstantly() {
		Account account = new Account(100);
		int balanceBefore = account.balance();
		account.deposit(10);
		assertEquals(balanceBefore + 10, account.balance());
	}

	@Test
	public void testDepositIllegalValues() {
		Account account = new Account(100);
		int balanceBefore = account.balance();
		account.deposit(-10);
		assertEquals(balanceBefore, account.balance());
	}

	@Test
	public void testWithdrawIllegalValues() {
		Account account = new Account(100);
		int balanceBefore = account.balance();
		account.withdraw(-10);
		assertEquals(balanceBefore, account.balance());
	}
}
