
import static org.junit.Assert.*;

import org.junit.Test;

public class BankTest {

	@Test
	public void testBankInitializesWithZeroCustomers() {
		Bank bank = new Bank();
		assertEquals(0, bank.getNumberOfCustomers());
	}

	@Test
	public void testAddCustomerIncreaseNumbers() {
		Bank bank = new Bank();
		bank.addCustomer("Ronnie", 1000);
		int customersBefore = bank.getNumberOfCustomers();
		bank.addCustomer("Rynolds", 200);
		bank.addCustomer("Barrie", 200);
		int customersAfter = bank.getNumberOfCustomers();
		assertEquals(customersAfter - customersBefore, 2);
	}

	@Test
	public void testRemovingCustomerDecreasesNumber() {
		Bank bank = new Bank();
		bank.addCustomer("Ronnie", 1000);
		bank.addCustomer("Rynolds", 200);
		int customersBefore = bank.getNumberOfCustomers();
		bank.removeCustomer("Ronnie");
		int customersAfter = bank.getNumberOfCustomers();
		assertEquals(customersBefore - customersAfter, 1);
	}

	@Test
	public void testMoneyTransferBetweenCustomers() {
		Bank bank = new Bank();
		bank.addCustomer("Ronnie", 1000);
		bank.addCustomer("Rynolds", 200);
		Customer fromCustomer = bank.getCustomers().get(0);
		Customer toCustomer = bank.getCustomers().get(1);

		bank.transferMoney(fromCustomer, toCustomer, 100);
		assertEquals(300, toCustomer.getAccount().balance());
		assertEquals(900, fromCustomer.getAccount().balance());
	}

}
