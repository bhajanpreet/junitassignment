
import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void testCreateCustomer() {
		Customer customer = new Customer("Ronnie", 1000);
		assertEquals(true, customer.getAccount() instanceof Account);
		Customer customer1 = new Customer("Eddie", 200);
		assertEquals(true, customer1.getAccount() instanceof Account);
	}

	@Test
	public void testIfCustomerCanBeCreatedWithInitAmount() {
		Customer customer = new Customer("Ronnie", 1000);
		assertEquals(1000, customer.getAccount().balance());
	}

	@Test
	public void testIfCustomerCanBeCreatedWithZeroAmountBalance() {
		try {
			Customer customer1 = new Customer("James", 0);
			assertEquals(0, customer1.getAccount().balance());
		} catch (Exception e) {
			fail("Some error encountered while creating customer with zero dollars in account");
		}
	}

}
